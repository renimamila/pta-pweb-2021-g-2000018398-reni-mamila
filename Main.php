<!DOCTYPE html>
<html>
	<head>
		<title>Tugas Akhir Pemrograman Web</title>
		<link rel="stylesheet" type="text/css" href="styleUK.css">
	</head>
	<body>

		<!-- bagian header -->
		<header class="warna">
			<div class="container">
				<h1><a href="">RENI MAMILA</a></h1>
				<ul>
					<li><a href="#about">About</a></li>
					<li><a href="#portfolio">Portfolio</a></li>
					<li><a href="#fitur">Fitur</a></li>
					<li><a href="#pr">SiswaPrestasi</a></li>
					<li><a href="#contact">Contact</a></li>
					<li><a href="Saran.php">Kritik/Saran</a></li>
				</ul>
				

			</div>
		</header>

		<!-- bagian banner -->
		<section class="banner">
			<div class="container">
				<div class="banner-left">
					<h2>Hai...<br>
						Saya adalah seorang bloger<span class="efek-ngetik"></span></h2>
					<p>Selamat datang di website Tugas Akhir saya </p>
				</div>
			</div>
		</section>
		<main>
		<!-- bagian about -->
		<section id="about">
			<div class="container">
				<h3>About</h3>
				&nbsp;&nbsp;&nbsp;<P>Assalamualaikum wr.wb.
					Nama saya Reni Mamila, dan Reni nama panggilan saya. Saya lahir di Probolinggo Jawa Timur pada tanggal 25 Oktober 2002. 
					Saya adalah anak ketiga dari 4 bersaudara. Saya terlahir dari keluarga yang sangat sederhana. 
					Sejak kecil ayah selalu menasehati agar rajin beribadah, bersikap jujur, dan baik terhadap sesama. 
					Ketika berumur 4 tahun, saya mulai bersekolah di TK Nusa Indah, kemudian melanjutkan di SD Bucor Kulon 1 pada tahun 2007,
					kemudian setelah lulus SD ayah dan ibu memutuskan untuk melanjutkan pendidikan SMP-SMA saya di pesantren, tepatnya di pondok
					pesantren terbesar di Jawa Timur. Pada saat di SMA saya sering mengikuti lomba dan festival pulic speaking, karena saya sangat 
					senang tampil didepan public. Bermusik merupakan hobi saya sejak aku kecil. Sekarang saya sedang menempuh program Sarjana dengan 
					prodi Teknik Informatika di Universitas Ahmad Dahlan. Di UAD saya mengikuti MAPALA. Tapi sayang dimasa pandemi ini tim MAPALA
					belum bisa explore ke luar.</p>
			</div>
		</section>

		<!-- bagian portfolio -->
		<section id="portfolio">
			<div class="container">
				<h3>Pictures</h3>
				<div class="col-3">
					<a href="">
						<img class="size-picture"src="reni.jpg">
					</a>
				</div>

				<div class="col-3">
					<a href="">
						<img class="size-picture"src="reni.jpg">
					</a>
				</div>

				<div class="col-3">
					<a href="">
						<img class="size-picture"src="dsg.png">
					</a>
				</div>

				</div>
		</section>

		<section id="fitur">
			<div class="container">
				<h3>Fitur</h3>
				<div class="col-3">
					<a href="TotalBelanja.html">
						<img class="size-picture"src="shopping.jpg">
						<p>Shopping</p>
					</a>
				</div>

				<div class="col-3">
					<a href="calculator.html">
						<img class="size-picture"src="calculator.jpg">
						<p>Calculator</p>
					</a>
				</div>

				<div class="col-3">
					<a href="registrasi.html">
						<img class="size-picture"src="student.jpg">
						<p>Registrasi Calon Siswa Berperestasi</p>
					</a>
				</div>

				</div>
		</section>

		<section id="pr">
			<div class="container">
				<h3>Siswa Berprestasi</h3>
		<?php 
		$arrNama = array ("RENI MAMILA ","RANTI SUISTIRA PERTIWI","STEPHANNY ELSA DAMARA","AWALLUL FAJRI RAHMAN"); 

		echo "<h1>Nama-nama Siswa Berprestasi Angkatan 2020 : </h1><br>"; 
		for ($i=0; $i<count($arrNama); $i++) { 
			echo $arrNama[$i] ."<br>"; 
		} 
		
		?> 
		</div>
		</section>

		<!-- bagian contact -->
		<section id="kontak">
			<div class="container">
				<h3>Contact</h3>
				<div class="col-4">
					<h4>Alamat</h4>
					<p>RT 002 RW 001, Glagah, Pakuniran, Probolinggo, Jawa Timur</p>
				</div>

				<div class="col-4">
					<h4>Email</h4>
					<p>raanky.ryansyah.@gmail.com</p>
				</div>

				<div class="col-4">
					<h4>Telp/Hp</h4>
					<p>+62 8525 8255 009</p>
				</div>	

                <div class="col-4">
					<h4>Media Sosial</h4>
                    <p> <a href="https://www.instagram.com/ree.rnt/">INSTAGRAM</a></p>
				</div>	
                
			</div>
		</section>
	</main>

		<!-- bagian footer -->
		<footer>
			<div class="container">
				<small>Dibuat &copy; 2021 - Reni Mamila</small>
			</div>
		</footer>

		<script src="text.js"></script>
	</body>
</html>